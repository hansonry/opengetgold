#ifndef __TILEDATA_H__
#define __TILEDATA_H__
#include "SDL.h"
#include <stdbool.h>

struct imagemap
{
   const char * name;
   const char * filename;
   int x;
   int y;
};

struct tiledata_rend
{
   SDL_Texture * text;
   SDL_Rect src;
};

struct tiledata_logic
{
   bool passable;
   bool exit;
};

void tiledata_init(void);
void tiledata_destroy(void);

void tiledata_loadtextures(SDL_Renderer * rend);

void tiledata_add(const char * name, const char * texture, 
                  int text_x, int text_y, bool passable, bool exit);

void tiledata_addlogic(const char * name, bool passable, bool exit);

void tiledata_insertmap(struct imagemap * map);

int tiledata_getindex(const char * name);
const char * tiledata_getname(int index);
void tiledata_gettile(int index, 
                      struct tiledata_logic * logic, 
                      struct tiledata_rend * rend);

size_t tiledata_getcount(void);

#endif // __TILEDATA_H__

