#ifndef __LEVELIO_H__
#define __LEVELIO_H__
#include "map.h"
#include "tiledata.h"

void levelio_save(const char * filename, struct map * map);
void levelio_load(struct map * map,  const char * filename);



#endif // __LEVELIO_H__


