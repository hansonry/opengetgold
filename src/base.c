#include <stdio.h>
#include "base.h"
#include "common_def.h"
#include "map.h"
#include "isorenderer.h"
#include "image.h"
#include "tiledata.h"
#include "levelio.h"
#include "autogen/staticTilesIndex.h"

enum enginestate
{
   e_es_editing,
   e_es_playing
};

struct editor
{
   struct pos pos_cursor;
   int hotslots[10];
   int selected_slot;
};


enum keys
{
   e_k_x_pos,
   e_k_x_neg,
   e_k_z_pos,
   e_k_z_neg,
   e_k_size
};


struct player
{
   struct pos pos;
   struct pos target;
   int keys[e_k_size];
};

struct game
{
   struct player player;
   float timer_tick_seconds;
   float timeout_tick_seconds;
   float tick_percent;
};


//    y
//    +
//    |
//    *
//   / \
//  +   +
// z     x
static void toScreenCoords(int wx, int wy, int wz, int * sx, int *sy)
{
   if(sx != NULL)
   {
      *sx = wx * 32 - wz * 32;
   }

   if(sy != NULL)
   {
      *sy = wx * 16 + wz * 16 - wy * 59;
   }
}

static SDL_Keycode keycodes[] = {
   SDLK_KP_3, // e_k_x_pos
   SDLK_KP_7, // e_k_x_neg,
   SDLK_KP_1, // e_k_z_pos
   SDLK_KP_9, // e_k_z_neg,
};


static SDL_Texture * text_player;
static SDL_Texture * text_cursor;
static SDL_Texture * text_shadow;
static SDL_Texture * text_mask;
static SDL_Rect rect_cursor;
static struct map * map;

static enum enginestate state;
static struct editor editor;
static struct game   game;

static bool isexit(int x, int y, int z)
{
   int tileIndex;
   struct tiledata_logic logic;
   tileIndex = map_get(map, x, y, z);
   if(tileIndex < 0)
   {
      return false;
   }
   tiledata_gettile(tileIndex, &logic, NULL);
   return logic.exit;
}

static bool ispassable(int x, int y, int z)
{
   int tileIndex;
   struct tiledata_logic logic;
   tileIndex = map_get(map, x, y, z);
   if(tileIndex < 0)
   {
      return true;
   }
   tiledata_gettile(tileIndex, &logic, NULL);
   return logic.passable;  
}

static void resetplayer(struct player * player)
{
   int i;
   player->pos.x = 1;
   player->pos.y = 1;
   player->pos.z = 0;
   POS_COPY(player->target, player->pos);
   
   // Reset game keys
   for(i = 0; i < e_k_size; i++)
   {
      player->keys[i] = 0;
   }
}

static bool hasfallenoffmap(struct player * player)
{
   struct pos min;
   map_getbounds(map, &min, NULL);
   return (player->pos.y < (min.y - 5)) ? true : false;
}


void base_init(SDL_Renderer * rend)
{
   int i;


   text_cursor = image_loadIntoVRAM(rend, "assets/cursor.png", &rect_cursor.w, &rect_cursor.h);
   text_player = image_loadIntoVRAM(rend, "assets/player.png", NULL, NULL);
   text_shadow = image_loadIntoVRAM(rend, "assets/shadow.png", NULL, NULL);
   text_mask   = image_loadIntoVRAM(rend, "assets/Mask.png",   NULL, NULL);

   isorenderer_init(rend, text_shadow, text_mask);
   tiledata_init();

   tiledata_addlogic("BasicBlock",    false, false);
   tiledata_addlogic("FourColumn",    false, false);
   tiledata_addlogic("FourColumnTop", false, false);
   tiledata_addlogic("BlueClayBlock", false, false);
   tiledata_addlogic("ConcreteBlock", false, false);
   tiledata_addlogic("ConcreteOct",   false, false);
   tiledata_addlogic("ConcreteExit",  true,  true);


   tiledata_insertmap(StaticImageIndex);

   tiledata_loadtextures(rend);




   map = map_new();

   map_set(map, 2, 0, 0, tiledata_getindex("FourColumn"));
   map_set(map, 2, 1, 0, tiledata_getindex("FourColumnTop"));
   map_set(map, 1, 0, 0, tiledata_getindex("BasicBlock"));

   editor.pos_cursor.x = 0;
   editor.pos_cursor.y = 0;
   editor.pos_cursor.z = 0;
   for(i = 0; i < 10; i++)
   {
      if(i < tiledata_getcount())
      {
         editor.hotslots[i] = i;
      }
      else
      {
         editor.hotslots[i] = 0;
      }
   }
   editor.selected_slot = 0;
   state = e_es_editing;


   // Game
   resetplayer(&game.player);
   
   game.timeout_tick_seconds = 0.25;
   //game.timeout_tick_seconds = 1;
   
}

void base_cleanup(void)
{
   int i;
   map_delete(map);
   tiledata_destroy();
   SDL_DestroyTexture(text_player);
   SDL_DestroyTexture(text_cursor);
   SDL_DestroyTexture(text_shadow);
   SDL_DestroyTexture(text_mask);
   isorenderer_cleanup();
}

bool base_event(SDL_Event * event)
{
   int i;

   switch(state)
   {
   case e_es_editing:
   
      if(event->type == SDL_KEYDOWN)
      {
         // ======== Editor Keys ============ //
         switch(event->key.keysym.sym)
         {
         case SDLK_KP_PLUS:
            editor.pos_cursor.y ++;
            break;
         case SDLK_KP_MINUS:
            editor.pos_cursor.y --;
            break;
         case SDLK_KP_3:
            editor.pos_cursor.x ++;
            break;
         case SDLK_KP_7:
            editor.pos_cursor.x --;
            break;
         case SDLK_KP_1:
            editor.pos_cursor.z ++;
            break;
         case SDLK_KP_9:
            editor.pos_cursor.z --;
            break;
         case SDLK_ESCAPE:
            return false;
            break;
         case SDLK_LEFT:
            if(editor.selected_slot == 0)
            {
               editor.selected_slot = 9;
            }
            else
            {
               editor.selected_slot --;
            }
            break;
         case SDLK_RIGHT:
            if(editor.selected_slot == 9)
            {
               editor.selected_slot = 0;
            }
            else
            {
               editor.selected_slot ++;
            }
            break;
         case SDLK_UP:
            if(editor.hotslots[editor.selected_slot] == tiledata_getcount() - 1)
            {
               editor.hotslots[editor.selected_slot] = 0;
            }
            else
            {
               editor.hotslots[editor.selected_slot] ++;
            }
            break;
         case SDLK_DOWN:
            if(editor.hotslots[editor.selected_slot] == 0)
            {
               editor.hotslots[editor.selected_slot] = tiledata_getcount() - 1;
            }
            else
            {
               editor.hotslots[editor.selected_slot] --;
            }
            break;
         case SDLK_SPACE:
            if(map_get(map, POS_EXP(editor.pos_cursor)) == -1)
            {
               map_set(map, POS_EXP(editor.pos_cursor), editor.hotslots[editor.selected_slot]);
            }
            else
            {
               map_clear(map, POS_EXP(editor.pos_cursor));
            }
            break;
         case SDLK_p:
            state = e_es_playing;
            break;
         case SDLK_s:
            levelio_save("map.oggm", map);
            break;
         case SDLK_l:
            levelio_load(map, "map.oggm");
            break;
         }
      }
      break;         
   case e_es_playing:
      // ======== Game Keys ============ //         


      if(event->type == SDL_KEYDOWN)
      {
         switch(event->key.keysym.sym)
         {
         case SDLK_ESCAPE:
            return false;
            break;
         case SDLK_p:
            state = e_es_editing;
            break;
         case SDLK_r:
            resetplayer(&game.player);
            break;
         }
         
      }
      
      if(event->type == SDL_KEYDOWN || event->type == SDL_KEYUP)
      {
         for(i = 0; i < e_k_size; i++)
         {
            if(event->key.keysym.sym == keycodes[i])
            {
               break;
            }
         }
         
         if(i < e_k_size )
         {
            if(event->type == SDL_KEYDOWN)
            {
               if((game.player.keys[i] & 1) == 0)
               {
                  game.player.keys[i] |= 2;
               }
               game.player.keys[i] |= 1;
            }
            else
            {
               game.player.keys[i] &= ~1;
            }
         }
      }
      break;
      
   }
   return true;
}

void base_update(double seconds)
{
   struct pos new_pos;
   struct pos min;
   int i;
   
   if(state == e_es_playing)
   {
      game.timer_tick_seconds += seconds;
      if(game.timer_tick_seconds > game.timeout_tick_seconds)
      {
         game.timer_tick_seconds -= game.timeout_tick_seconds;
         // ======= Tick Code Here ======= //
        

         POS_COPY(game.player.pos, game.player.target); 
         

         // Setup for checking falling
         POS_COPY(new_pos, game.player.pos);
         new_pos.y --;

         
         // Check for falling off the level
         if(hasfallenoffmap(&game.player))
         {
            resetplayer(&game.player);
         }
         // Check for falling
         else if(ispassable(POS_EXP(new_pos)))
         {
            POS_COPY(game.player.target, new_pos);
         }
         else
         {
            // Check for motion
            POS_COPY(new_pos, game.player.pos);
            if(game.player.keys[e_k_x_pos])
            {
               new_pos.x ++;
            }
            else if(game.player.keys[e_k_x_neg])
            {
               new_pos.x --;
            }
            else if(game.player.keys[e_k_z_pos])
            {
               new_pos.z ++;
            }
            else if(game.player.keys[e_k_z_neg])
            {
               new_pos.z --;
            }
            
            if(ispassable(POS_EXP(new_pos)))
            {
               POS_COPY(game.player.target, new_pos);
            }
         }
         
         // Reset game keys
         for(i = 0; i < e_k_size; i++)
         {
            game.player.keys[i] &= ~2;
         }
      }

      game.tick_percent = game.timer_tick_seconds / game.timeout_tick_seconds;
      
      // Check for Exit Gate
      if(isexit(POS_EXP(game.player.target)) && game.tick_percent > 0.4)
      {
         resetplayer(&game.player);
      }
   }
   else
   {
      game.tick_percent = 0;
   }
   
}

static void drawTilePicker(SDL_Renderer * rend, int x, int y, int * w, int * h)
{
   // Draw Background
   SDL_Rect r;
   SDL_Rect src;
   size_t i;
   struct tiledata_rend tdrend;
   
   r.x = x;
   r.y = y;
   r.w = 10 * 74 + 10;
   r.h = 111;
   
   if(w != NULL)
   {
      *w = r.w;
   }
   if(h != NULL)
   {
      *h = r.h;
   }

   SDL_RenderFillRect(rend, &r);
   SDL_SetRenderDrawColor(rend, 255, 0, 0, 255);
   SDL_RenderDrawRect(rend, &r);


   // Draw Selector


   r.x = x + editor.selected_slot * 74 + 5;
   r.y = y + 5;
   r.w = 74;
   r.h = 101;
   SDL_RenderDrawRect(rend, &r);
   // Draw hotslots
   r.w = src.w = 64;
   r.h = src.h = 91;

   for(i = 0; i < 10; i++)
   {
      tiledata_gettile(editor.hotslots[i], NULL, &tdrend);
      r.x = x + 10 + i * 74;
      r.y = y + 10;
      SDL_RenderCopy(rend, tdrend.text, &tdrend.src, &r);
   }

}

void base_render(SDL_Renderer * rend)
{
   struct maptile * tileList;
   struct tiledata_rend tdrend;
   size_t i, count;
   size_t k;
   SDL_Rect src;

   SDL_SetRenderDrawColor(rend, 0, 0, 0, 255);

   SDL_RenderClear(rend);


   src.w = 64;
   src.h = 91;
   src.x = 0;
   src.y = 0;

   // Render Map
   tileList = map_gettiles(map, &count);
   for(i = 0; i < count; i++)
   {      
      if(tileList[i].tileIndex >= 0)
      {
         tiledata_gettile(tileList[i].tileIndex, NULL, &tdrend);
         isorenderer_addtile(tdrend.text, &tdrend.src, POS_EXP(tileList[i].pos));
      }
   }
   

   // Render Player
   if(state == e_es_playing)
   {
      isorenderer_addplayer(text_player, NULL, 
                            POS_EXP(game.player.pos), 
                            POS_EXP(game.player.target), 
                            game.tick_percent);
   }
   
   isorenderer_render(rend);

   if(state == e_es_editing)
   {
      // Render Cursor

      toScreenCoords(POS_EXP(editor.pos_cursor), &rect_cursor.x, &rect_cursor.y);
      rect_cursor.x -= 1;
      rect_cursor.y -= 1;
      SDL_RenderCopy(rend, text_cursor, NULL, &rect_cursor);

      // Render Selector
      drawTilePicker(rend, 5, 360, NULL, NULL);
   }
}


