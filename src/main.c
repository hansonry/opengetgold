#include <stdio.h>
#include <stdbool.h>
#include "base.h"
#include "SDL.h"
#include "SDL_image.h"


int main(int argc, char * args[])
{
   SDL_Window * window;
   SDL_Renderer * rend;
   SDL_Event event;
   bool running;
   Uint64 simLastTicks, newTicks, frameLastTicks;
   double seconds;
   int targetFPS = 60;
   Uint64 targetFrameDelay = SDL_GetPerformanceFrequency() / targetFPS;
   Sint64 frameDiff;


   SDL_Init(SDL_INIT_EVERYTHING);

   window = SDL_CreateWindow("Open Get Gold",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             640, 480, 0);

   rend = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

   base_init(rend);   
   
   simLastTicks = frameLastTicks = SDL_GetPerformanceCounter();
   running = true;
   while(running)
   {
      while(SDL_PollEvent(&event))
      {
         if(event.type == SDL_QUIT)
         {
            running = false;
         }

         if(base_event(&event) == false)
         {
            running = false;
         }
      }
      
      // Updating
      newTicks = SDL_GetPerformanceCounter();
      seconds = ((newTicks - simLastTicks) / (double)SDL_GetPerformanceFrequency());
      simLastTicks = newTicks;
      base_update(seconds);

      // Rendering
      base_render(rend);

      SDL_RenderPresent(rend);

      // Compute delay time needed

      newTicks = SDL_GetPerformanceCounter();
      frameDiff = targetFrameDelay - (newTicks - frameLastTicks);
      if(frameDiff > 0)
      {
         SDL_Delay((frameDiff * 1000) / SDL_GetPerformanceFrequency());
      }
      frameLastTicks = SDL_GetPerformanceCounter();
   }
   base_cleanup();


   SDL_DestroyRenderer(rend);
   SDL_DestroyWindow(window);

   SDL_Quit();
   printf("End\n");
   return 0;
}

