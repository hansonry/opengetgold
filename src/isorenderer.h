#ifndef __ISORENDERER_H__
#define __ISORENDERER_H__
#include "SDL.h"

void isorenderer_init(SDL_Renderer * rend, 
                      SDL_Texture * text_shadow,
                      SDL_Texture * text_mask);
void isorenderer_cleanup();

void isorenderer_addtile(SDL_Texture * text, SDL_Rect * src, 
                         int x, int y, int z);
                     
void isorenderer_addplayer(SDL_Texture * text, SDL_Rect * src, 
                           int x, int y, int z, 
                           int tx, int ty, int tz,
                           float percent);


void isorenderer_render(SDL_Renderer * rend);

#endif // __ISORENDERER_H__
