#include <stdlib.h>
#include <string.h>
#include "tiledata.h"
#include "image.h"

#define GROWBY 32

struct td_texture
{
   SDL_Texture * text;
   char * filename;
};

struct td_tile
{
   char * name;
   size_t texture_index;
   SDL_Rect src;
   struct tiledata_logic logic;
};

struct tiledata
{
   struct td_texture * text_base;
   size_t text_size;
   size_t text_count;

   struct td_tile * tile_base;
   size_t tile_size;
   size_t tile_count; 
};


static struct tiledata td;

void tiledata_init(void)
{
   td.text_base = malloc(sizeof(struct td_texture) * GROWBY);
   td.text_count = 0;
   td.text_size = GROWBY;

   td.tile_base = malloc(sizeof(struct td_tile) * GROWBY);
   td.tile_count = 0;
   td.tile_size = GROWBY;
}

void tiledata_destroy(void)
{
   size_t i;
   for(i = 0; i < td.text_count; i++)
   {
      free(td.text_base[i].filename);
      if(td.text_base[i].text != NULL)
      {
         SDL_DestroyTexture(td.text_base[i].text);
      }
   }
   free(td.text_base);

   for(i = 0; i < td.tile_count; i++)
   {
      free(td.tile_base[i].name);
   }
   free(td.tile_base);
}

void tiledata_loadtextures(SDL_Renderer * rend)
{
   size_t i;
   for(i = 0; i < td.text_count; i++)
   {
      if(td.text_base[i].text == NULL)
      {
         td.text_base[i].text = 
            image_loadIntoVRAM(rend, td.text_base[i].filename, NULL, NULL);
      }
   }
}

static size_t tiledata_gettexture(const char * filename)
{
   size_t i;
   for(i = 0; i < td.text_count; i++)
   {
      if(strcmp(filename, td.text_base[i].filename) == 0)
      {
         break;
      }
   }

   if(i >= td.text_count)
   {
      if(td.text_count >= td.text_size)
      {
         td.text_size = td.text_count + GROWBY;
         td.text_base = realloc(td.text_base, 
                                sizeof(struct td_texture) * td.text_size);
      }
      i = td.text_count;
      td.text_count ++;

      td.text_base[i].filename = strdup(filename);
      td.text_base[i].text = NULL;
   }
   return i;
}

static struct td_tile * tiledata_addrawtile(void)
{
   struct td_tile * tile;
   if(td.tile_count >= td.tile_size)
   {
      td.tile_size = td.tile_count + GROWBY;
      td.tile_base = realloc(td.tile_base, 
                             sizeof(struct td_tile) * td.tile_size);
   }

   tile = &td.tile_base[td.tile_count];
   td.tile_count ++;
   return tile;
}

void tiledata_add(const char * name, const char * texture, 
                  int text_x, int text_y, bool passable, bool exit)
{
   struct td_tile * tile;
   tile = tiledata_addrawtile();
   tile->name = strdup(name);
   tile->texture_index = tiledata_gettexture(texture);
   tile->src.x = text_x;
   tile->src.y = text_y;
   tile->src.w = 64;
   tile->src.h = 91;

   tile->logic.passable = passable;
   tile->logic.exit = exit;
}

void tiledata_addlogic(const char * name, bool passable, bool exit)
{
   struct td_tile * tile;
   tile = tiledata_addrawtile();
   tile->name = strdup(name);
   tile->texture_index = -1;
   tile->src.x = 0;
   tile->src.y = 0;
   tile->src.w = 64;
   tile->src.h = 91;

   tile->logic.passable = passable;
   tile->logic.exit = exit;
}

void tiledata_insertmap(struct imagemap * map)
{
   size_t i;
   int index;
   struct td_tile * tile;
   i = 0;
   while(map[i].name != NULL)
   {
      index = tiledata_getindex(map[i].name);
      if(index >= 0)
      {
         tile = &td.tile_base[index];
         tile->texture_index = tiledata_gettexture(map[i].filename);
         tile->src.x = map[i].x;
         tile->src.y = map[i].y;
      }
      i++;
   }
}

int tiledata_getindex(const char * name)
{
   int i;
   i = -1;
   for(i = 0; i < td.tile_count; i++)
   {
      if(strcmp(td.tile_base[i].name, name) == 0)
      {
         break;
      }
   }
   return i;
}


const char * tiledata_getname(int index)
{
   if(index >= 0 && index < td.tile_count)
   {
      return td.tile_base[index].name;
   }
   return NULL;
}

void tiledata_gettile(int index, 
                      struct tiledata_logic * logic, 
                      struct tiledata_rend * rend)
{
   if(logic != NULL)
   {
      memcpy(logic, &td.tile_base[index].logic, sizeof(struct tiledata_logic)); 
   }

   if(rend != NULL)
   {
      memcpy(&rend->src, &td.tile_base[index].src, sizeof(SDL_Rect));
      if(td.tile_base[index].texture_index == -1)
      {
         rend->text = NULL;
      }
      else
      {
         rend->text = td.text_base[td.tile_base[index].texture_index].text;
      }
   }
}


size_t tiledata_getcount(void)
{
   return td.tile_count;
}

