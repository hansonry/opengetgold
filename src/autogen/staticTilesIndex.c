#include <stddef.h>
#include "tiledata.h"
#include "staticTilesIndex.h"

struct imagemap StaticImageIndex[] = {
   { "BasicBlock", "assets/staticTiles.png", 0, 0 },
   { "FourColumn", "assets/staticTiles.png", 64, 0 },
   { "FourColumnTop", "assets/staticTiles.png", 128, 0 },
   { "BlueClayBlock", "assets/staticTiles.png", 192, 0 },
   { "GoldStand", "assets/staticTiles.png", 256, 0 },
   { "ConcreteBlock", "assets/staticTiles.png", 320, 0 },
   { "ConcreteOct", "assets/staticTiles.png", 384, 0 },
   { "ConcreteExit", "assets/staticTiles.png", 448, 0 },
   { "ConcreteExitClose", "assets/staticTiles.png", 0, 91 },
   { NULL, NULL, 0, 0 }
};

