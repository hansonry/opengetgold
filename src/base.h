#ifndef __BASE_H__
#define __BASE_H__
#include <stdbool.h>
#include "SDL.h"

void base_init(SDL_Renderer * rend);

bool base_event(SDL_Event * event);

void base_update(double seconds);

void base_render(SDL_Renderer * rend);

void base_cleanup(void);

#endif // __BASE_H__

