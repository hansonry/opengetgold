#ifndef __COMMON_DEF_H__
#define __COMMON_DEF_H__


struct pos
{
   int x;
   int y;
   int z;
};

#define POS_EXP(p) (p).x, (p).y, (p).z
#define POS_COPY(dest, src) (dest).x = (src).x; (dest).y = (src).y; (dest).z = (src).z

#endif // __COMMON_DEF_H__

