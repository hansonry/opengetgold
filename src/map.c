#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "map.h"


struct map
{
   struct maptile * base;
   size_t size;
   size_t count;
   struct pos min;
   struct pos max;
   bool recompute_bounds;
};

#define GROW_BY 16

struct map *  map_new(void)
{
   struct map * map;
   map = malloc(sizeof(struct map));

   map->size = GROW_BY;
   map->base = malloc(sizeof(struct maptile) * GROW_BY);
   map->count = 0;

   map->recompute_bounds = true;

   return map;
}

void map_delete(struct map * map)
{
   free(map->base);
   free(map);
}

void map_clearall(struct map * map)
{
   map->count = 0;
}

struct maptile * map_find(struct map * map, int x, int y, int z, size_t * index)
{
   struct maptile * tile;
   size_t i;

   tile = NULL;
   for(i = 0; i < map->count; i++)
   {
      if(map->base[i].pos.x == x &&
         map->base[i].pos.y == y &&
         map->base[i].pos.z == z)
      {
         if(index != NULL)
         {
            *index = i;
         }
         tile = &map->base[i];
         break;
      }
   }
   return tile;
}

void map_clear(struct map * map, int x, int y, int z)
{
   size_t index;
   struct maptile * tile;
   
   tile = map_find(map, x, y, z, &index);

   if(tile == NULL)
   {
      return;
   }

   map->count --;
   memcpy(&map->base[index], &map->base[map->count], sizeof(struct maptile));
   
   map->recompute_bounds = true;
   
}

static int map_tilecmp(struct pos * p1, struct pos * p2)
{
   if(p1->y == p2->y)
   {
      return (p1->x + p1->z) - (p2->x + p2->z);
   }
   return p1->y - p2->y;
}

void map_set(struct map * map, int x, int y, int z, int tileIndex)
{
   struct maptile * tile;
   struct maptile newTile;
   size_t i;

   tile = map_find(map, x, y, z, NULL);

   if(tile == NULL)
   {
      if(map->count >= map->size)
      {
         map->size = map->count + GROW_BY;
         map->base = realloc(map->base, sizeof(struct maptile) * map->size);
      }

      tile = &map->base[ map->count];
      
      tile->pos.x = x;
      tile->pos.y = y;
      tile->pos.z = z;
      
      map->recompute_bounds = true;

      map->count ++;
   }

   tile->tileIndex = tileIndex;
}

struct maptile * map_gettiles(struct map * map, size_t * size)
{
   if(size != NULL)
   {
      *size = map->count;
   }
   return map->base;
}


int map_get(struct map * map, int x, int y, int z)
{
   struct maptile * tile;
   tile = map_find(map, x, y, z, NULL);

   if(tile != NULL)
   {
      return tile->tileIndex;
   }
   return -1;
}


void map_getbounds(struct map * map, struct pos * min, struct pos * max)
{
   int i;
   struct maptile * tile;
   if(map->recompute_bounds)
   {
      map->recompute_bounds = false;
      
      map->min.x = 0;
      map->min.y = 0;
      map->min.z = 0;

      map->max.x = 0;
      map->max.y = 0;
      map->max.z = 0;  
      
      for(i = 0; i < map->count; i++)
      {
         tile = &map->base[i];
         if(tile->pos.x > map->max.x)
         {
            map->max.x = tile->pos.x;
         }
         if(tile->pos.x < map->min.x)
         {
            map->min.x = tile->pos.x;
         }

         if(tile->pos.y > map->max.y)
         {
            map->max.y = tile->pos.y;
         }
         if(tile->pos.y < map->min.y)
         {
            map->min.y = tile->pos.y;
         }
         
         if(tile->pos.z > map->max.z)
         {
            map->max.z = tile->pos.z;
         }
         if(tile->pos.z < map->min.z)
         {
            map->min.z = tile->pos.z;
         }
      }
   }
   
   
   if(min != NULL)
   {
      POS_COPY(*min, map->min);
   }
   
   if(max != NULL)
   {
      POS_COPY(*max, map->max);
   }
}


int map_findtileindex(struct map * map, int tileIndex)
{
   size_t i;
   for(i = 0; i < map->count; i++)
   {
      if(map->base[i].tileIndex == tileIndex)
      {
         return i;
      }
   }

   return -1;
}


void map_remaptiles(struct map * map, int * mapping)
{
   size_t i;
   for(i = 0; i < map->count; i++)
   {
      map->base[i].tileIndex = mapping[map->base[i].tileIndex];
   }
}

int map_isinsidebounds(struct map * map, int x, int y, int z)
{
   return x >= map->min.x && x <= map->max.x &&
          y >= map->min.y && y <= map->max.y &&
          z >= map->min.z && z <= map->max.z;
}
