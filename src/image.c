#include <stdio.h>
#include "image.h"
#include "SDL_image.h"


SDL_Texture * image_loadIntoVRAM(SDL_Renderer * rend, 
                                 const char * filename,
                                 int * width,
                                 int * height)
{
   SDL_Texture * text;
   SDL_Surface * surf;
   surf = IMG_Load(filename);
   if(surf != NULL)
   {
      text = SDL_CreateTextureFromSurface(rend, surf);
      
      if(width != NULL)
      {
         *width = surf->w;
      }

      if(height != NULL)
      {
         *height = surf->h;
      }

      SDL_FreeSurface(surf);
      return text;
   }

   fprintf(stderr, "IMG_Load: %s\n", IMG_GetError());
   return NULL;
}

