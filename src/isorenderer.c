#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "isorenderer.h"
#include "common_def.h"
#include "sig.h"

#define GROW_BY 16

#define PLAYER_WINDOW_WIDTH  128
#define PLAYER_WINDOW_HEIGHT 182

enum ir_element_type
{
   e_iret_block,
   e_iret_player,
   e_iret_gold,
};

enum ir_element_drawtype
{
   e_iredt_none,
   e_iredt_normal,
   e_iredt_smalltransparent
};

struct ir_element
{
   size_t nextIndex;
   struct pos pos;
   SDL_Texture * text;
   SDL_Rect source;
   SDL_Rect destination;
   bool is_null_source;
   bool casts_shadow;
   bool recevies_shadow;
   enum ir_element_type type;
   enum ir_element_drawtype drawtype;
   bool inshadow;
   
};

struct isorenderer
{
   int offset_x;
   int offset_y;
   struct ir_element * eles_base;
   size_t eles_count;
   size_t eles_size;
   size_t start_index;
   SDL_Texture * text_shadow;
   SDL_Texture * text_mask;
   SDL_Texture * text_player_window;
   SDL_Texture * text_player_window_masked;
   int xray_focus_index;
};


static struct isorenderer ir;



//    y
//    +
//    |
//    *
//   / \
//  +   +
// z     x
static void isorenderer_toScreenCoords(int wx, int wy, int wz, int * sx, int *sy)
{
   if(sx != NULL)
   {
      *sx = wx * 32 - wz * 32;
   }

   if(sy != NULL)
   {
      *sy = wx * 16 + wz * 16 - wy * 59;
   }
}


void isorenderer_init(SDL_Renderer * rend, 
                      SDL_Texture * text_shadow,
                      SDL_Texture * text_mask)
{
   
   ir.offset_x = 0;
   ir.offset_y = 0;
   
   ir.eles_base = malloc(sizeof(struct ir_element) * GROW_BY);
   ir.eles_size = GROW_BY;
   ir.eles_count = 0;
   
   ir.start_index = 0;
   
   ir.text_shadow = text_shadow;
   ir.text_mask = text_mask;

   ir.xray_focus_index = -1;

   ir.text_player_window = SDL_CreateTexture(rend, SDL_PIXELFORMAT_RGBA8888, 
                                                   SDL_TEXTUREACCESS_TARGET,
                                                   PLAYER_WINDOW_WIDTH,
                                                   PLAYER_WINDOW_HEIGHT);
   SDL_SetTextureBlendMode(ir.text_player_window, SDL_BLENDMODE_MOD);

   ir.text_player_window_masked = SDL_CreateTexture(rend, SDL_PIXELFORMAT_RGBA8888, 
                                                          SDL_TEXTUREACCESS_TARGET,
                                                          PLAYER_WINDOW_WIDTH,
                                                          PLAYER_WINDOW_HEIGHT);
   SDL_SetTextureBlendMode(ir.text_player_window_masked, SDL_BLENDMODE_BLEND);
}

void isorenderer_cleanup()
{
   SDL_DestroyTexture(ir.text_player_window);
   SDL_DestroyTexture(ir.text_player_window_masked);
   free(ir.eles_base);
}

static int isorenderer_poscmp(struct pos * p1, struct pos * p2)
{
   int v1, v2;
   
   v1 = p1->x + p1->z;
   v2 = p2->x + p2->z;
   
   if(v1 == v2)
   {
      return p1->y - p2->y;
   }
   return v1 - v2;
   
}


static size_t isorenderer_insert(struct ir_element * element)
{
   struct ir_element * mem;
   size_t i, p;
   size_t newIndex;
   // Grow the number of elements
   if(ir.eles_count >= ir.eles_size)
   {
      size_t new_size = ir.eles_count + GROW_BY;
      // Loop though and update all the sizes
      
      for(i = 0; i < ir.eles_count; i++)
      {
         if(ir.eles_base[i].nextIndex == ir.eles_count)
         {
            ir.eles_base[i].nextIndex = new_size;
         }
      }
      
      // Resize the buffer
      ir.eles_base = realloc(ir.eles_base, 
                             sizeof(struct ir_element) * new_size);
      ir.eles_size = new_size;
   }
   
   newIndex = ir.eles_count;
   if(ir.eles_count == 0)
   {
      ir.start_index = 0;
      element->nextIndex = ir.eles_size;
   }
   else
   {
      p = ir.start_index;
      for(i = ir.start_index; i < ir.eles_count; i = ir.eles_base[i].nextIndex)
      {
         if(isorenderer_poscmp(&ir.eles_base[i].pos, &element->pos) > 0)
         {
            break;
         }
         p = i;
      }
      
      if(i == ir.start_index)
      {
         element->nextIndex = ir.start_index;
         ir.start_index = newIndex;         
      }
      else
      {
         element->nextIndex = i;
         ir.eles_base[p].nextIndex = newIndex;
      }
   }
   

   mem = &ir.eles_base[newIndex];
   ir.eles_count++;  
   memcpy(mem, element, sizeof(struct ir_element));
   return newIndex;
}

static void isorenderer_addcomon(struct ir_element * element,
                                 SDL_Texture * text, SDL_Rect * src)
{
   element->text = text;
   if(src != NULL)
   {
      element->source.x = src->x;
      element->source.y = src->y;
      element->source.w = src->w;
      element->source.h = src->h;
      element->is_null_source = false;
      element->destination.w = src->w;
      element->destination.h = src->h;
   }
   else
   {
      element->destination.w = 64;
      element->destination.h = 91;
      element->is_null_source = true;
   }
}

void isorenderer_addtile(SDL_Texture * text, SDL_Rect * src, 
                         int x, int y, int z)
{
   struct ir_element element;
   
   isorenderer_addcomon(&element, text, src);
   
   
   isorenderer_toScreenCoords(x, y, z, 
                              &element.destination.x, 
                              &element.destination.y);
   
   element.pos.x = x;
   element.pos.y = y;
   element.pos.z = z;
   
   element.casts_shadow = true;
   element.recevies_shadow = true;
   element.type = e_iret_block;
   
   (void)isorenderer_insert(&element);
}

void isorenderer_addplayer(SDL_Texture * text, SDL_Rect * src, 
                           int x, int y, int z, 
                           int tx, int ty, int tz,
                           float percent)
{
   int x1, x2, y1, y2;
   struct ir_element element;
   isorenderer_addcomon(&element, text, src);
   
   
   isorenderer_toScreenCoords(x,  y,  z,  &x1, &y1);
   isorenderer_toScreenCoords(tx, ty, tz, &x2, &y2);
                              
   
   element.destination.x = sig_intlerp(x1, x2, percent);
   element.destination.y = sig_intlerp(y1, y2, percent);
   
   if(percent > 0.5)
   {
      element.pos.x = tx;
      element.pos.y = ty;
      element.pos.z = tz;
   }
   else
   {
      element.pos.x = x;
      element.pos.y = y;
      element.pos.z = z;
   }
   
   element.casts_shadow = false;
   element.recevies_shadow = false;
   element.type = e_iret_player;

   ir.xray_focus_index = (int)isorenderer_insert(&element);
}



static void isorender_setdrawtype(enum ir_element_drawtype drawtype)
{
   int i;
   for(i = 0; i < ir.eles_count; i++)
   {
      ir.eles_base[i].drawtype = drawtype;
   }
}

struct ir_element * isorender_getelementat(int x, int y, int z)
{
   int i;
   for(i = 0; i < ir.eles_count; i++)
   {
      if(ir.eles_base[i].pos.x == x &&
         ir.eles_base[i].pos.y == y &&
         ir.eles_base[i].pos.z == z)
      {
         return &ir.eles_base[i];
      }
   }
   return NULL;
}

static void isorender_recursivescan(struct pos * start, int left)
{
   struct ir_element * element;

   if(left <= 0 )
   {
      return;
   }

   element = isorender_getelementat(POS_EXP(*start));

   if(element == NULL)
   {
   }
}


#define FILLINDEX(x, z) ((x) + (z) * 5)
struct ir_map
{
   struct ir_element * element;
   bool token;
};

static void isorender_seek(struct ir_map map[25], const struct pos seek[2])
{
   int i, index;
   bool stuck;
   struct pos attempt;
   struct pos pos = {0, 0, 0};

   stuck = false;
   while(!stuck)
   {
      index = FILLINDEX(pos.x + 2, pos.z + 2);
      map[index].token = true;

      for(i = 0 ; i < 2; i++)
      {
         POS_COPY(attempt, pos);
         attempt.x += seek[i].x;
         attempt.y += seek[i].y;
         attempt.z += seek[i].z;
         if(attempt.x >= -2 && attempt.x <= 2 &&
            attempt.z >= -2 && attempt.z <= 2)
         {
            index = FILLINDEX(attempt.x + 2, attempt.z + 2);
            if(map[index].element == NULL ||
               map[index].element->type != e_iret_block)
            {
               POS_COPY(pos, attempt);
               break;         
            }
         }
      }

      if(i >= 2)
      {
         stuck = true;
      }
   }
}

static void isorender_setxraydrawflags(struct ir_element * xray_focus)
{
   struct ir_element * element;
   int i, k;
   int hasmore;
   int fx, fz;
   int lx, lz;
   struct ir_map map[25];
   const struct pos seek_left[2] = {
      {  0, 0, 1 },
      { -1, 0, 0 }
   };

   const struct pos seek_right[2] = {
      { 1, 0,  0 },
      { 0, 0, -1 }
   };

   // Clear the temparary map
   for(i = 0; i < 25; i++)
   {
      map[i].element = NULL;
      map[i].token = false;
   }

   
   isorender_setdrawtype(e_iredt_none);
   
   // Pick a Column of elements to render and populate the map
   for(i = 0; i < ir.eles_count; i++)
   {
      element = &ir.eles_base[i];
      if(element->pos.x - element->pos.z <= xray_focus->pos.x - xray_focus->pos.z + 2 &&
         element->pos.x - element->pos.z >= xray_focus->pos.x - xray_focus->pos.z - 2)
      {
         element->drawtype = e_iredt_normal;
         
         fx = element->pos.x - xray_focus->pos.x;
         fz = element->pos.z - xray_focus->pos.z;
         if(fx >= -2 && fx <= 2 && fz >= -2 && fz <= 2 && 
            element->pos.y == xray_focus->pos.y)
         {
            k = FILLINDEX(fx + 2, fz + 2);
            map[k].element = element; 
         }
      }

   }

   // Seek left and right
   // This populates the map with tokens
   isorender_seek(map, seek_right);
   isorender_seek(map, seek_left);




   // Find all the tokens on the map and clear twards the center
   // from each token.
   for(fz = 0; fz < 5; fz++)
   {
      for(fx = 0; fx < 5; fx++)
      {
         k = FILLINDEX(fx, fz);
         if(map[k].token)
         {
            lx = fx - 2 + xray_focus->pos.x;
            lz = fz - 2 + xray_focus->pos.z;
            for(i = 0; i < ir.eles_count; i++)
            {
               element = &ir.eles_base[i];
               if(element->drawtype == e_iredt_normal &&
                  element->pos.x + element->pos.z >= lx + lz &&
                  element->pos.x - element->pos.z == lx - lz &&
                  element->pos.y >= xray_focus->pos.y)
               {
                  element->drawtype = e_iredt_smalltransparent;
               }
            }
         }
      }
   }
   
   xray_focus->drawtype = e_iredt_normal;
}

static void isorender_computeshadow(void)
{
   struct ir_element * element, * shader;
   int i, k;

   for(i = ir.start_index; i < ir.eles_count; i = ir.eles_base[i].nextIndex)
   {
      element = &ir.eles_base[i];

      element->inshadow = false;

      for(k = ir.eles_base[i].nextIndex; k < ir.eles_count; k = ir.eles_base[k].nextIndex)
      {
         shader = &ir.eles_base[k];
         if(shader->casts_shadow &&
            shader->pos.x == element->pos.x &&
            shader->pos.z == element->pos.z)
         {
            if(shader->pos.y > element->pos.y + 1)
            {
               element->inshadow = true;
            }
            break;
         }
      }
   }
}

static void isorenderer_pass(SDL_Renderer * rend, 
                             int offset_x, int offset_y)
{
   struct ir_element * element, * shader;
   int i, k;
   SDL_Rect * src;
   SDL_Rect dest;

   for(i = ir.start_index; i < ir.eles_count; i = ir.eles_base[i].nextIndex)
   {
      element = &ir.eles_base[i];

      if(element->drawtype == e_iredt_normal ||
         element->drawtype == e_iredt_smalltransparent)
      {
         if(element->is_null_source)
         {
            src = NULL;
         }
         else
         {
            src = &element->source;
         }

         if(element->drawtype == e_iredt_normal)
         {
            SDL_SetTextureAlphaMod(element->text,  255);
            SDL_SetTextureAlphaMod(ir.text_shadow, 255);
            dest.x = element->destination.x + offset_x;
            dest.y = element->destination.y + offset_y;
            dest.w = element->destination.w;
            dest.h = element->destination.h;
         }
         else
         {
            SDL_SetTextureAlphaMod(element->text,  100);
            SDL_SetTextureAlphaMod(ir.text_shadow, 100);
            dest.x = element->destination.x + offset_x + (element->destination.w / 4);
            dest.y = element->destination.y + offset_y + (element->destination.h / 4);
            dest.w = element->destination.w / 2;
            dest.h = element->destination.h / 2;
         }

         SDL_RenderCopy(rend, element->text, src, &dest);
         
         // Draw Shadow
         if(element->recevies_shadow && element->inshadow)
         {
            SDL_RenderCopy(rend, ir.text_shadow, NULL, &dest);
         }
      }
   }
}

void isorenderer_render(SDL_Renderer * rend)
{
   struct ir_element * xray_focus;
   SDL_Rect view;
   int player_window_x, player_window_y;

   // Compute Shadows
   isorender_computeshadow();

   // Checking for a player to xray
   if(ir.xray_focus_index >= 0)
   {
      xray_focus = &ir.eles_base[ir.xray_focus_index];
   }
   else
   {
      xray_focus = NULL;
   }

   if(xray_focus == NULL)
   {
      isorender_setdrawtype(e_iredt_normal);
      isorenderer_pass(rend, 0, 0);
   }
   else
   {
      
      // Calculate where the texture will hang
      player_window_x = xray_focus->destination.x - xray_focus->destination.w / 2;
      player_window_y = xray_focus->destination.y - xray_focus->destination.h / 2;

      // Render XRay Texture
      SDL_SetRenderTarget(rend, ir.text_player_window);
      SDL_SetRenderDrawColor(rend, 0, 0, 0, 0);
      SDL_RenderClear(rend);
      isorender_setxraydrawflags(xray_focus);
      isorenderer_pass(rend, -player_window_x, -player_window_y);

      // Render Masked Texture
      SDL_SetRenderTarget(rend, ir.text_player_window_masked);
      SDL_SetTextureBlendMode(ir.text_mask, SDL_BLENDMODE_NONE);
      SDL_RenderCopy(rend, ir.text_mask, NULL, NULL);
      SDL_RenderCopy(rend, ir.text_player_window, NULL, NULL);


      // Render Again but this time put the new masked texture on top
      SDL_SetRenderTarget(rend, NULL);

      isorender_setdrawtype(e_iredt_normal);
      isorenderer_pass(rend,  0, 0);

      view.x = player_window_x;
      view.y = player_window_y;
      view.w = PLAYER_WINDOW_WIDTH;
      view.h = PLAYER_WINDOW_HEIGHT;
   
      SDL_RenderCopy(rend, ir.text_player_window_masked, NULL, &view);
      
      //isorender_setxraydrawflags(xray_focus);
      //isorenderer_pass(rend, 0, 0);
   }


   // Reset Everything
   ir.eles_count = 0;
   ir.xray_focus_index = -1;
}

