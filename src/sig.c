#include "sig.h"

int sig_intlerp(int p1, int p2, float percent)
{
   int delta;
   if(percent <= 0)
   {
      return p1;
   }

   if(percent >= 1)
   {
      return p2;
   }

   delta = p2 - p1;
   return p1 + (int)(delta * percent);
}

