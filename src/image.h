#ifndef __IMAGE_H__
#define __IMAGE_H__
#include "SDL.h"


SDL_Texture * image_loadIntoVRAM(SDL_Renderer * rend, 
                                 const char * filename,
                                 int * width,
                                 int * height);

#endif // __IMAGE_H__

