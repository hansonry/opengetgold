#ifndef __MAP_H__
#define __MAP_H__
#include <stddef.h>
#include "common_def.h"

struct maptile
{
   struct pos pos;
   int tileIndex;
};


struct map;

struct map *  map_new(void);
void map_delete(struct map * map);

void map_clearall(struct map * map);
void map_clear(struct map * map, int x, int y, int z);
void map_set(struct map * map, int x, int y, int z, int tileIndex);

struct maptile * map_gettiles(struct map * map, size_t * size);

int map_get(struct map * map, int x, int y, int z);

void map_getbounds(struct map * map, struct pos * min, struct pos * max);

int map_findtileindex(struct map * map, int tileIndex);

void map_remaptiles(struct map * map, int * mapping);

int map_isinsidebounds(struct map * map, int x, int y, int z);

#endif // __MAP_H__

