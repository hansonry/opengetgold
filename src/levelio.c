#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "levelio.h"

static void levelio_write32(FILE * fh, uint32_t value)
{
   uint8_t buffer[4];
   buffer[0] = value & 0xFF;
   value = value >> 8;
   buffer[1] = value & 0xFF; 
   value = value >> 8;
   buffer[2] = value & 0xFF; 
   value = value >> 8;
   buffer[3] = value & 0xFF; 

   fwrite(buffer, 1, 4, fh);
}

static void levelio_writestring(FILE *fh, const char * str)
{
   size_t strl;
   uint8_t size;

   strl = strlen(str);
   if(strl > 0xFF)
   {
      size = 0xFF;
   }
   else
   {
      size = (uint8_t)strl;
   }

   fwrite(&size, 1, 1, fh);
   fwrite(str, 1, size, fh);

   
}

static int levelio_read32(uint32_t * value, FILE * fh)
{
   uint8_t buffer[4];
   uint32_t v;
   if(fread(buffer, 1, 4, fh) != 4)
   {
      return 1;
   }

   v = 0;
   v = v | buffer[3];
   v = v << 8;
   v = v | buffer[2];
   v = v << 8;
   v = v | buffer[1];
   v = v << 8;
   v = v | buffer[0];
   
   if(value != NULL)
   {
      *value = v;
   }

   return 0;
}

static int levelio_readstring(char * str, FILE *fh)
{
   uint8_t str_size;
   
   if(str == NULL)
   {
      return 1;
   }

   if(fread(&str_size, 1, 1, fh) != 1)
   {
      return 1;
   }

   if(fread(str, 1, str_size, fh) != str_size)
   {
      return 1;
   }

   str[str_size] = '\0';

   return 0;
}

#define MAGICNUMBER 0x10DEDA28

void levelio_save(const char * filename, struct map * map)
{
   FILE * fh;
   struct maptile * tiles;
   size_t size, i, tiledata_count;
   const char * str;

   int * indexmap;
   int nextindex;
   int foundIndex;
   fh = fopen(filename, "wb");
   if(fh == NULL)
   {
      fprintf(stderr, "levelio_save: Unable to open file \"%s\" for writting.\n", filename);
      return;
   }

   // Create Mapping
   tiles = map_gettiles(map, &size);
   tiledata_count = tiledata_getcount();
   indexmap = calloc(tiledata_count, sizeof(int));
   nextindex = 0;

   for(i = 0; i < tiledata_count; i++)
   {
      foundIndex = map_findtileindex(map, i);
      if(foundIndex != -1)
      {
         indexmap[i] = nextindex;
         nextindex ++;
      }
      else
      {
         indexmap[i] = -1;
      }
   }


   // Write Headder

   levelio_write32(fh, MAGICNUMBER); // Write Magic Number
   levelio_write32(fh, size);
   levelio_write32(fh, nextindex);

   // Write Level

   for(i = 0; i < size; i++)
   {
      // Write Tile
      levelio_write32(fh, tiles[i].pos.x);
      levelio_write32(fh, tiles[i].pos.y);
      levelio_write32(fh, tiles[i].pos.z);
      levelio_write32(fh, indexmap[tiles[i].tileIndex]);
   }

   // Write Pallet
   for(i = 0; i < tiledata_count; i++)
   {
      if(indexmap[i] >= 0)
      {
         str = tiledata_getname(i);
         levelio_writestring(fh, str);
      }

   }

   fclose(fh);
   free(indexmap);
}



static int levelio_readtile(struct pos * pos, int * tileIndex, FILE *fh)
{
   struct pos _pos;
   int _tileIndex;
   int result;
   uint32_t value;
   result = 0;

   if(levelio_read32(&value, fh))
   {
      result = 1;
   }
   _pos.x = (int)value;

   if(levelio_read32(&value, fh))
   {
      result = 1;
   }
   _pos.y = (int)value;

   if(levelio_read32(&value, fh))
   {
      result = 1;
   }
   _pos.z = (int)value;

   if(levelio_read32(&value, fh))
   {
      result = 1;
   }
   _tileIndex = (int)value;

   if(result == 0)
   {
      if(pos != NULL)
      {
         POS_COPY(*pos, _pos);
      }
      if(tileIndex != NULL)
      {
         *tileIndex = _tileIndex;
      }
   }

   return result;

   
}

void levelio_load(struct map * map,  const char * filename)
{
   FILE * fh;
   struct maptile * tiles;
   size_t size, i;
   uint32_t value;
   struct pos pos;
   int tileIndex;
   size_t palletsize;
   int * indexmap;
   char str[256];

   fh = fopen(filename, "rb");
   if(fh == NULL)
   {
      fprintf(stderr, "levelio_load: Unable to open file \"%s\" for reading.\n", filename);
      return;
   }


   // Read Headder
   
   // Read Magic Number
   if(levelio_read32(&value, fh))
   {
      fprintf(stderr, "levelio_load: Failed to read header, file \"%s\" may not be a map file\n", filename);
      fclose(fh);
      return;
   }
   
   if(value != MAGICNUMBER)
   {
      fprintf(stderr, "levelio_load: Magic Number does not match, file \"%s\" may not be a map file.\n", filename);
      fclose(fh);
      return;
   }

   // Read Level Size
   if(levelio_read32(&value, fh))
   {
      fprintf(stderr, "levelio_load: Failed to read header, file \"%s\" may be corrupt\n", filename);
      fclose(fh);
      return;
   }

   size = (size_t)value;
   
   
   // Read Pallet Size
   if(levelio_read32(&value, fh))
   {
      fprintf(stderr, "levelio_load: Failed to read header, file \"%s\" may be corrupt\n", filename);
      fclose(fh);
      return;
   }
   palletsize = (size_t)value;


   map_clearall(map);
   // Read Level
   for(i = 0; i < size; i++)
   {
      if(levelio_readtile(&pos, &tileIndex, fh))
      {
         fprintf(stderr, "levelio_load: Failed to read level tile at index %d, file \"%s\" may be corrupt\n", (int)i, filename);
         fclose(fh);
         return;
      }
      map_set(map, POS_EXP(pos), tileIndex);
   }

   // Read Pallet
   if(palletsize > 0)
   {
      indexmap = calloc(palletsize, sizeof(int));
      for(i = 0; i < palletsize; i++)
      {
         if(levelio_readstring(str, fh))
         {
            fprintf(stderr, "levelio_load: Failed to read level pallet at index %d, file \"%s\" may be corrupt\n", (int)i, filename);
            free(indexmap);
            fclose(fh);
            return;
         }
         indexmap[i] = tiledata_getindex(str);
      }

      map_remaptiles(map, indexmap);
      free(indexmap);
   }

   fclose(fh);
}

