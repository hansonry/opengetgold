# Level Format

Makeing levels is hard work and takes a lot of verification play thoughs.
Becuase of this it is important for it to be easy to port levels from
one version to another of the game. For this reason each level compoent
has a name in the form of a string.  This is the driving idea behind
the level format.



## Specification

The level files are binary and stored in little endian and there will be
no spacing between the data (tight paking).

The level file has 3 parts and the are listed in order:

1. Header
2. Level
3. Pallet

The Pallet Data size varies due to the diffrent string lengths, so it
is the last section. It is possible to compute where the Pallet data should 
start as each Level Entry is the same size and the Headder specifies 
the number of Level Entries.

The Pallet Data is optional. If the Number of Pallet Entries is 0, then
the game should assume that the Tile Index are in refrence to the 
current loaded tile set. This is not desireable thougth.

### Header

The header has 3 parts and is 12 bytes long:

1. Magic Number             (unsigned 4-bytes):  0x10DEDA28  (LOADDASH)
3. Number of Level Entries  (unsigned 4-bytes)
2. Number of Pallet Entries (unsigned 4-bytes)

### Level

The level Entry list contains a set of Level Entries that give the position
and type of each level tile.

In the future there may be an additional entry to indicate a tile variant.

Each record of the level index is 16 bytes long and looks like the following:

1. X Coordinate (signed 4-bytes)
2. Y Coordinate (signed 4-bytes)
3. Z Coordinate (signed 4-bytes)
4. Tile Index   (signed 4-bytes)


### Pallet

The pallet is a list that maps a Level Tile Index to a Name. This way
names can be remapped when loaded. This makes the level format more
robust to tile data changes.

The Pallet entrys order is important. The first Palet Entry will be Pallet
Entry 0 and the second Pallet Entry 1 and so on... The Level section
will refrence these entries by index.

Each Pallet Entry will consist of a variable length string. The string
will start with a 1-byte size and N charactes after the size byte.

For now the characters will be ASCII only.

Each Pallet Entry will look like the following:

1. String Size (unsigned 1-byte)
2. String Data (N-bytes based on the String Size)

