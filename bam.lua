settings = NewSettings()

if family == "windows" then
   -- LIB SDL
   settings.cc.includes:Add("lib/SDL2-2.0.10/include")
   settings.link.flags:Add("-Xlinker /SUBSYSTEM:CONSOLE")
   settings.link.libpath:Add("lib/SDL2-2.0.10/lib/x64")   
   settings.link.libs:Add("SDL2")
   settings.link.libs:Add("SDL2main")
   
   -- LIB SDL_image
   settings.cc.includes:Add("lib/SDL2_image-2.0.5/include")
   settings.link.libpath:Add("lib/SDL2_image-2.0.5/lib/x64")   
   settings.link.libs:Add("SDL2_image")
   
   -- Get rid of deprecated warnings
   settings.cc.defines:Add(" _CRT_SECURE_NO_WARNINGS")
   settings.cc.defines:Add("strdup=_strdup")
   
else
   settings.cc.flags:Add("`sdl2-config --cflags`")
   settings.link.flags:Add("`sdl2-config --libs`")
   settings.link.libs:Add("SDL2_image")
end


settings.cc.includes:Add("src")

sources = Collect("src/*.c", "src/autogen/*.c")
objects = Compile(settings, sources)
exe = Link(settings, "opengetgold", objects)

-- Copy all the DLL files into the current working directory if we are
-- on windows
if family == "windows" then
   AddDependency(exe, CopyToDirectory("",  
      "lib/SDL2-2.0.10/lib/x64/SDL2.dll",
      "lib/SDL2_image-2.0.5/lib/x64/SDL2_image.dll",
      "lib/SDL2_image-2.0.5/lib/x64/zlib1.dll",
      "lib/SDL2_image-2.0.5/lib/x64/libjpeg-9.dll",
      "lib/SDL2_image-2.0.5/lib/x64/libpng16-16.dll",
      "lib/SDL2_image-2.0.5/lib/x64/libtiff-5.dll",
      "lib/SDL2_image-2.0.5/lib/x64/libwebp-7.dll"))
end


