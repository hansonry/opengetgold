import bpy
import math
import os

print("Rendering Static Tiles")

# Dissable Everything
CHILDEREN = bpy.context.window.view_layer.layer_collection.children
for col in CHILDEREN:
   col.exclude = True

CHILDEREN["Setup"].exclude = False
CHILDEREN["Tiles"].exclude = False
bpy.data.collections["Setup"].hide_render = False
tilesColection = bpy.data.collections["Tiles"]
tilesColection.hide_render = False


# Hide Everything first
for i in tilesColection.objects:
    bpy.data.objects[i.name].hide_render = True

imageWidth = 64
imageHeight = 91
staticImageTileWidth = 8
variableName = "StaticImageIndex"
staticIndexFilename = bpy.path.abspath("//../src/autogen/staticTilesIndex.c")
directory = os.path.dirname(staticIndexFilename)
if not os.path.exists(directory):
   os.makedirs(directory)
print("static Index Filename: %s" % staticIndexFilename)
filehandle = open(staticIndexFilename, "w")
filehandle.write("#include <stddef.h>\n")
filehandle.write("#include \"tiledata.h\"\n");
filehandle.write("#include \"%s\"\n\n"  % "staticTilesIndex.h")

filehandle.write("struct imagemap %s[] = {\n" % variableName); 

# Render each tile
index = 0;
for i in tilesColection.objects:
    bpy.data.objects[i.name].hide_render = False
    filename  = "//isoTilesRender/%04d.%s.png" % (index, i.name)
    print("Rendering %s" % i.name)
    x = (index % staticImageTileWidth) * imageWidth;
    y = math.floor(index / staticImageTileWidth) * imageHeight;
    filehandle.write("   { \"%s\", \"assets/staticTiles.png\", %d, %d },\n" % (i.name, x, y))
    bpy.context.scene.render.filepath = filename
    bpy.ops.render.render(write_still = True)
    bpy.data.objects[i.name].hide_render = True
    index = index + 1


filehandle.write("   { NULL, NULL, 0, 0 }\n")
filehandle.write("};\n\n")
filehandle.close();

staticIndexFilename = bpy.path.abspath("//../src/autogen/staticTilesIndex.h")

filehandle = open(staticIndexFilename, "w")
filehandle.write("#ifndef __STATICTILESINDEX_H__\n")
filehandle.write("#define __STATICTILESINDEX_H__\n\n")

# filehandle.write("struct imagemap\n{\n   const char * name;\n   int x;\n   int y;\n};\n\n")

filehandle.write("extern struct imagemap %s[];\n\n" % variableName); 
filehandle.write("#endif // __STATICTILESINDEX_H__\n\n")
filehandle.close();

