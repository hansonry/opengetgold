#!/bin/bash

cd "$(dirname "$0")"

# Delete old directory
rm -rf isoTilesRender

# Render all the tiles into a directory
# Each frame shows a new tile to render
blender -b isoTiles.blend -P BlenderRenderStaticTiles.py

# merge all the tiles into one picture
montage -tile 8x -geometry +0+0 -background none isoTilesRender/* ../assets/staticTiles.png


